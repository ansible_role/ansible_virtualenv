*******
LXD driver installation guide
*******

Requirements
============

* LXD

Install
=======

No additional virtualenv packages required. Apt installs necessary requirements
while installing ``lxd``.
